Début de compile textes licences 

## [ACAB](https://github.com/jgrey4296/acab/blob/main/LICENSE)
Une liecence écrite par John Grey en 2020. Ici l'acronyme ACAB n'invoque pas des bâtard mais signe le nom d'une architecture Python “ ”“Analytic Collaborative Agent Bureaucracy”.
Ici le contrat interdit l'usage de l'œuvre ou des produits de l'œuvre
* aux entités chargées de l'application de la loi, institutions carcérales ou de l'immigration 
* aux entités commerciales où le ratio de rémunération (salarié, freelance, actions ou autres avantages) entre la personne la plus élevée et la personne la plus basse de l'entité est supérieur à 50 : 1 
* aux entités commerciales où le conseil d'administration ou la direction ne comprend pas au moins une moitié de personnes de couleurs 

## [Genderfail protest fonts](http://genderfailpress.com/protest-fonts.html)

## [WTFPL – Do What the Fuck You Want to Public Licens](http://www.wtfpl.net)

## [Creative commons](https://creativecommons.org/licenses/?lang=fr)

## [CC4R](https://constantvzw.org/wefts/cc4r.fr.html)
